FROM existenz/webstack:7.4

RUN apk add --no-cache curl \
					git \
					php7 \
					php7-fpm \
					php7-curl \
					php7-dom \
					php7-fileinfo \
					php7-gd \
					php7-iconv \
					php7-imagick \
					php7-json \
					php7-mbstring \
					php7-openssl \
					php7-pdo \
					php7-pdo_mysql \
					php7-phar \
					php7-session \
					php7-simplexml \
					php7-sockets \
					php7-tokenizer \
					php7-xml \
					php7-xmlwriter \
					php7-zip

RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/v3.13/community/ --allow-untrusted gnu-libiconv=1.15-r3

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
